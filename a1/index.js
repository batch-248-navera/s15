console.log("Hello World!");

let firstName = "Paul";
console.log("First Name:" + firstName);

let lastName= "Navera";
console.log("Last Name:" + lastName);

let myAge= 26;
console.log("Age: " + myAge);

let hobbies = ["Programming", "Playing Basketball","Watching Anime"];
console.log("Hobbies: ")
console.log (hobbies);

let workAddress = {
         houseNumber: 123 ,
         street: "Purok 1",
         city: "Legaspi",
         state: "Phillipines",

}

console.log("Work Address:")
console.log(workAddress);



let completeName = "Steve Rogers";
console.log("My full name is " + completeName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {

    username:"captain_america",
    fullName: "Steve Rogers",
    age: 40,
    isActive: false,

}
console.log("My Full Profile: ")
console.log(profile);

let fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

let lastLocation = "Arctic Ocean";
lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);

